class binarysearchtree:

    def __init__(self, node):
        self.node = node
        self.right = None
        self.left = None

    def insertion(self,val):
        if self.node is None:
            self.node = val
            return
        if self.node == val:
            return
        if self.node > val:
            if self.left:
                self.left.insertion(val)
            else:
                self.left = binarysearchtree(val)
        else:
            if self.right:
                self.right.insertion(val)
            else:
                self.right = binarysearchtree(val)

    def search(self,data):
        if self.node == data:
            print("Node is found")
            return
        if self.node > data:
            if self.left:
                self.left.search(data)
            else:
                print("Node is not present in the tree")
        else:
            if self.right:
                self.right.search(data)
            else:
                print("Node is not present in the tree")

    def preorder(self):
        if self.node == None:
            print("The binary search tree is empty")
            return
        print(self.node, end="\n")
        if self.left:
            self.left.preorder()
        if self.right:
            self.right.preorder()

    def inorder(self):
        if self.node == None:
            print("The binary tree is empty")
            return
        if self.left:
            self.left.inorder()
        print(self.node, end = " ")
        if self.right:
            self.right.inorder()

    def postorder(self):
        if self.left:
            self.left.postorder()
        if self.right:
            self.right.postorder()
        print(self.node, end = " ")

    def delete(self, value):
        if self.node is None:
            print("The Binary tree is empty")
            return
        if self.node == value:
            self.node = None
            return
        if self.node > value:
            if self.left:
                self.left.delete(value)
            else:
                print("Node is not present in the Binary Tree")
            if self.right:
                self.right.delete(value)
            else:
                print("Node is not present in the Binary Tree")




root = binarysearchtree(None)

listt = [7,1,3,2,6,4,3,7,34,21,21]

for i in listt:
    root.insertion(i)

print("Pre Order")
root.preorder()
print()
print("In Order")
root.inorder()
print()
print("Post Order")
root.postorder()
print()
root.search(34)
root.delete(34)
root.search(34)
